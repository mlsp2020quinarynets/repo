#!/usr/bin/env python

import math
import random
import torch
import torch.nn.functional as F
from torch import nn
import numpy as np

def gaussianActivation(x, mu=0, sigma=1):
    return (1/(sigma*math.sqrt(2*math.pi)))*torch.exp((-1/2.)*((x-mu)/sigma)**2)

# For Xavier Normal initialization
def weights_init(m):
    if isinstance(m, nn.Conv2d):
        nn.init.xavier_normal_(m.weight, gain=np.sqrt(2))
        # nn.init.constant_(m.bias, 0.0)
    if isinstance(m, nn.Linear):
        nn.init.xavier_normal_(m.weight, gain=np.sqrt(2))
        nn.init.constant_(m.bias, 0.0)

class allConv_Net(nn.Module):
    """classifier model with only convolutional layers"""

    def __init__(self, nb_classes=10, in_channels=1):
        """Init FC classifier"""
        super(allConv_Net, self).__init__()
        self.nb_classes = nb_classes
        # self.mu = torch.nn.Parameter(torch.randn(1))
        # self.sigma = torch.nn.Parameter(torch.randn(1))
        self.mu = 0
        self.old_sigma = 1.
        self.old_mu = 0.
        self.sigma = 1.

        # Old index in case modification not good
        self.old_idx = ["classifier", 0]

        self.verbose = False


        # Convolutional layer 1
        self.conv_1 = nn.Conv2d(in_channels=in_channels, out_channels=25, kernel_size=5, stride=1, bias=False)

        # Convolutional layer 2
        self.conv_2 = nn.Conv2d(in_channels=25, out_channels=25, kernel_size=5, stride=2, bias=False)

        # Convolutional layer 3
        self.conv_3 = nn.Conv2d(in_channels=25, out_channels=8, kernel_size=3, stride=1, bias=False)

        # Convolutional layer 4
        out_channels = 3
        self.conv_4 = nn.Conv2d(in_channels=8, out_channels=out_channels, kernel_size=3, stride=2, bias=False)

        # Batch Normalization
        self.bn = torch.nn.BatchNorm2d(out_channels)

        # Convolutional layer 5
        self.conv_classifier = nn.Conv2d(in_channels=3, out_channels=nb_classes, kernel_size=3, stride=1, bias=False)
        self.old_weight_classifier = torch.ones_like(torch.Tensor([1]), dtype=torch.float32,  requires_grad=False)


        # Other params
        self.n_weights_conv_classifier = len(self.conv_classifier.weight.view(-1))
        self.n_gaussian = 2
        self.param_indices = list(range(self.n_weights_conv_classifier + self.n_gaussian))

    def freeze_layers(self):
        """
            Freeze all the layers except the last one (the classifier)
        """
        for p in self.conv_1.parameters():
            p.requires_grad = False
        for p in self.conv_2.parameters():
            p.requires_grad = False
        for p in self.conv_3.parameters():
            p.requires_grad = False
        for p in self.conv_4.parameters():
            p.requires_grad = False

    def try_modifying_a_weight(self, idx):
        # Copying weights in case modification not good
        if (idx[0] == "classifier"):
            self.old_weight_classifier = self.conv_classifier.weight.view(-1)[idx[1]].clone()
        elif (idx[0] == "gaussianActivation"):
            if (idx[1] == 0):
                self.old_sigma = self.sigma
            else:
                self.old_mu = self.mu
        else:
            raise NotImplementedError("try_modifying_a_weight function not implemented yet for {}".format(idx[0]))

        self.old_idx = idx

        if self.verbose:
            print("modifying at idx", self.old_idx)
            print("old weight:", self.old_weight)

        # Picking a new neighbor state
        # unif = torch.distributions.uniform.Uniform(-2, 2)
        if (idx[0] == "classifier"):
            # self.fc.weight.view(-1)[idx[1]] = torch.tensor(1, dtype=torch.float32, requires_grad=False).random_(-1, 2);
            self.conv_classifier.weight.view(-1)[idx[1]] = torch.tensor(1, dtype=torch.float32, requires_grad=False) * random.choice([-1., -0.5, 0., 0.5, 1.])

        elif (idx[0] == "gaussianActivation"):
            if (idx[1] == 0):
                #self.sigma = unif.sample()
                self.sigma = torch.tensor(1, dtype=torch.float32, requires_grad=False)*random.choice([i for i in np.arange(-2,2,0.1)])
            else:
                #self.mu = unif.sample()
                self.mu = torch.tensor(1, dtype=torch.float32, requires_grad=False)*random.choice([i for i in np.arange(-2,2,0.1)])

        else:
            raise NotImplementedError("try_modifying_a_weight function not implemented yet for {}".format(idx[0]))

        if self.verbose:
            print("new weight:", self.fc.weight.view(-1)[idx])

    def reject_last_modification(self):
        if self.verbose:
            print("rejecting modification made at idx", self.old_idx)
            print("restoring old weight", self.old_weight)
        if (self.old_idx[0] == "classifier"):
            self.conv_classifier.weight.view(-1)[self.old_idx[1]] = self.old_weight_classifier
        elif (self.old_idx[0] == "gaussianActivation"):
            if (self.old_idx[1] == 0):
                self.sigma = self.old_sigma
            else:
                self.mu = self.old_mu



    def forward(self, x):
        """Forward the FC classifier."""
        # print("mu: ", self.mu)
        # print("sigma: ", self.sigma)

        # Conv 1: Input of size 28x28x1, output 24x24x25
        x = self.conv_1(x)
        x = torch.nn.functional.leaky_relu(x)
        # print("Size of x after conv 1: ", x.shape)

        # Conv 2: Input of size 24x24x25, output 10x10x25
        x = self.conv_2(x)
        x = torch.nn.functional.leaky_relu(x)
        # print("Size of x after conv 2: ", x.shape)

        # Conv 3: Input of size 10x10x25, output 8x8x8
        x = self.conv_3(x)
        x = torch.nn.functional.leaky_relu(x)
        # print("Size of x after conv 3: ", x.shape)

        # Conv 4: Input of size 8x8x8, output 3x3x3
        x = self.conv_4(x)
        x = torch.nn.functional.leaky_relu(x)
        # print("Size of x after conv 4: ", x.shape)

        # Batch normalization
        x = self.bn(x)

        # Conv 5: Input of size 8x8x8, output 1x1xnb_classes
        x = self.conv_classifier(x)
        x = torch.nn.functional.leaky_relu(x)
        # print("Size of x after conv 5: ", x.shape)

        # Shape becomes 1xnb_classes
        x = x.view(-1, self.nb_classes)
        # print("Size of x after reshaping: ", x.shape)
        x = gaussianActivation(x, self.mu, self.sigma)
        output = F.log_softmax(x, dim=1)
        return output



if __name__=="__main__":
    print("Hello, World")
    n_samples = 5
    n_channels = 1
    sample_width = 28
    sample_height = 28
    input_tensor = torch.randn((n_samples, n_channels, sample_height, sample_width))
    model = allConv_Net(10)
    total_params = sum(p.numel() for p in model.parameters() if p.requires_grad)
    print("Total parameters to train of the model: ", total_params)
    output = model(input_tensor)
    print("Output size : ", output.size())
