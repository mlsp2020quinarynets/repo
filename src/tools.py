#!/usr/bin/env python3

import numpy as np
import torch

def dichotomie_decresent(deltas_E, acc_rate_start, acc_rate_end):

    ###########################################
    ###           function to compute the
    ###               solution of
    ###               an equation
    ###              by dichotomie
    ### In case of strictly decresent function
    ###########################################


    ##
    M = len(deltas_E)
    # function definition
    fun = lambda deltas, x : sum(np.exp(- x * np.array(deltas)))

    # The initial and final acceptation rate values
    Xi_inf = acc_rate_start
    Xi_sup = acc_rate_end
    # The tolerance of the found solutions for B_sup and B_inf
    d_sup =Xi_sup/100
    d_inf =Xi_inf/100

    M_Xi_sup = M*Xi_sup
    M_Xi_inf = M*Xi_inf

    # research interval [A-B] initialisation
    A = 0.0
    Beta_start = 1.0
    while fun(deltas_E, Beta_start) > Xi_sup/10:
        Beta_start = Beta_start*10

    B = Beta_start
    B_sup = Beta_start

    while (fun(deltas_E,B_sup) < M_Xi_sup - d_sup) or (fun(deltas_E,B_sup) > M_Xi_sup + d_sup):

        if (fun(deltas_E,B_sup) < M_Xi_sup - d_sup):
            B = B_sup
        else:
            A = B_sup

        B_sup = (A+B)/2

    ##
    # research interval [A-B] reset
    A = 0.0
    B = B_sup
    # initial value
    B_inf = 0.0


    while (fun(deltas_E, B_inf) < M_Xi_inf - d_inf) or (fun(deltas_E, B_inf) > M_Xi_inf + d_inf):

        if (fun(deltas_E,B_inf) < M_Xi_inf - d_inf):
            B = B_inf
        else:
            A = B_inf

        B_inf = (A+B)/2


    return (B_inf, B_sup)


def get_beta_schedule(n_iter_total, Beta_inf, Beta_sup, sigma, K):
    """ returns the list of Beta_values (one for each training iteration)
    """
    n = torch.arange(n_iter_total).float()
    Beta_inf = torch.Tensor([Beta_inf])
    Beta_sup = torch.Tensor([Beta_sup])
    Beta_values = Beta_inf*(Beta_sup/Beta_inf)**((1/(sigma-1))*(torch.ceil(n/K)-1))

    return Beta_values


def main():
    """ example to use the dichotomie_decresent function
    """
    deltas_E = [1, 2, 3, 5]
    (B_inf, B_sup) = dichotomie_decresent(deltas_E, 0.1, 0.01)
    print((B_inf, B_sup))

if __name__ == '__main__':
    main()
