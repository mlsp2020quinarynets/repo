#!/usr/bin/env python3

import torch
import numpy as np


def computeMetricsMulticlass(model, \
                   dataloader, \
                   criterion, \
                   device=torch.device('cpu')):
    """
        Compute the metrics of the model on a dataset

        Parameters
        ----------
        model: torch.Module
            model to compute metrics
        datLoader: torch.utils.data.dataloader.DataLoader
            DataLoader for the data
        criterion: torch.nn.modules.loss
            loss to use

        Returns
        -------
        accuracy: float
        precision: float
        recall: float
        f1_score: float
        error: float
        loss: It is not actually computed, so the result is not meaningful
    """
    nb_classes = model.nb_classes
    confusionMatrix = np.zeros((nb_classes, nb_classes)) # Each columns correspond to the class
    # predicted by the model and each row correspond to the true class

    loss_values_per_batch = 0
    nb_batch = 0
    model = model.double()
    model.eval()
    for points, labels in dataloader:
        nb_batch += 1
        data = points.double()
        data = data.to(device)
        labels = labels.to(device)
        outputs = model(data)

        # loss_values_per_batch += criterion(outputs, labels)

        # Getting the predicted classes
        cpu_labels = labels.cpu()
        for i in range(len(outputs)):
            predicted_class = int(outputs[i].max(0)[1])
            confusionMatrix[labels[i], predicted_class] += 1

    if (nb_classes == 2):
        TN = confusionMatrix[0,0]
        TP = confusionMatrix[1,1]
        FP = confusionMatrix[0,1]
        FN = confusionMatrix[1,0]
        avg_acc = (TN+TP)/(TP+TN+FP+FN)
        avg_precision = TP/(TP+FP)
        avg_recall = TP/(TP+FN)
        avg_f1_score = 2*avg_precision*avg_recall/(avg_precision+avg_recall)
        avg_error_rate = (FP+FN)/(TP+TN+FP+FN)
    else:
        TP = np.diag(confusionMatrix)
        FP = np.sum(confusionMatrix, axis=0) - TP
        FN = np.sum(confusionMatrix, axis=1) - TP
        TN = confusionMatrix.sum() - (FP + FN + TP)
        avg_acc = confusionMatrix.trace()/confusionMatrix.sum()
        precision =  TP/(TP+FP)
        avg_precision = precision.mean()
        recall = TP/(TP+FN)
        avg_recall = recall.mean()
        f1_score = 2*(recall*precision)/(recall+precision)
        avg_f1_score = f1_score.mean()
        error_rate = (confusionMatrix.sum()-confusionMatrix.trace())/(confusionMatrix.sum())
        avg_error_rate = error_rate.mean()

    loss = loss_values_per_batch/len(dataloader)

    return avg_acc, avg_precision, avg_recall, avg_f1_score, avg_error_rate, loss
