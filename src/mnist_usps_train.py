#!/usr/bin/env python

"""
    Code inspired from https://github.com/pytorch/examples/blob/master/mnist/main.py
"""

import os
import random
import numpy as np
import torch
import torch.optim as optim
from torch.utils.data.sampler import SubsetRandomSampler
from torchvision import datasets, transforms
from matplotlib import pyplot as plt
from model import allConv_Net, weights_init
from metrics import computeMetricsMulticlass


def plotLoss(loss_vals, test_loss_vals, nameLoss):
    """
        Plot the loss over the epochs

        Parameters
        ----------
        loss_vals: array
            Values of the loss to plot
        test_loss_vals: array
            Values of the test loss to plot
        nameLoss: str
            Path and name to save the figure
    """
    plt.figure()
    plt.plot(list(range(len(loss_vals))), loss_vals, label="train loss")
    plt.plot(list(range(len(test_loss_vals))), test_loss_vals, label="val_loss")
    plt.title("Loss")
    plt.xlabel('iteration')
    plt.ylabel('loss')
    plt.legend()
    inc = 0
    while os.path.isfile(nameLoss+str(inc)+'.png'):
        inc +=1
    plt.savefig(nameLoss+str(inc)+'.png')
    plt.close()


def train(model, device, train_loader, criterion, optimizer, epochs, lr_decay, nb_samples_train):
    """
        Function to train a model
    """
    model.train()
    loss_vals = []
    for epoch in range(epochs):
        print("Epoch {}".format(epoch))
        batch_idx = 0
        losses = []
        for data, target in train_loader:
            # Learning rate decay
            if (epoch%20 == 0 and epoch != 0):
                for group in optimizer.param_groups:
                    group['lr'] = lr_decay*group['lr']
            data, target = data.to(device), target.to(device)
            optimizer.zero_grad()

            # Forward pass
            output = model(data)
            loss = criterion(output, target)
            loss_data = loss.data.cpu()
            losses.append(loss_data.numpy())

            # Backward pass
            loss.backward()

            # Optimize: Updating weights
            optimizer.step()

            batch_idx += 1

        mean_train_loss = np.array(losses).mean()
        loss_vals.append(mean_train_loss)
    return loss_vals


def test(model, device, test_loader, nb_samples_test):
    """
        Function to test a model
    """
    model.eval()
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in test_loader:
            data, target = data.to(device), target.to(device)
            output = model(data)
            test_loss += torch.nn.functional.nll_loss(output, target, reduction='sum').item()  # sum up batch loss
            pred = output.argmax(dim=1, keepdim=True)  # get the index of the max log-probability
            correct += pred.eq(target.view_as(pred)).sum().item()

    test_loss /= nb_samples_test

    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
        test_loss, correct, nb_samples_test,
        100. * correct / nb_samples_test))


def data_load(train_batch_size=10, test_batch_size=10, dataset='MNIST', **kwargs):


    if (dataset == 'MNIST'):
        train_dataset = datasets.MNIST('../data',\
                                    train=True,\
                                    download=True,\
                                    transform=transforms.Compose([
                                                                    transforms.ToTensor(),
                                                                    transforms.Normalize((0.1307,), (0.3081,))
                                                                 ])
                                    )

        test_dataset = datasets.MNIST('../data',\
                                    train=False,\
                                    download=True,\
                                    transform=transforms.Compose([
                                                                    transforms.ToTensor(),
                                                                    transforms.Normalize((0.1307,), (0.3081,))
                                                                 ])
                                    )
    elif (dataset == 'USPS'):
        train_dataset = datasets.USPS('../data',\
                                    train=True,\
                                    download=True,\
                                    transform=transforms.Compose([
                                                                    transforms.Resize((28,28)), # Resize to have the same size as MNIST images
                                                                    transforms.ToTensor(),
                                                                    transforms.Normalize((0.1307,), (0.3081,))
                                                                 ])
                                    )

        test_dataset = datasets.USPS('../data',\
                                    train=False,\
                                    download=True,\
                                    transform=transforms.Compose([
                                                                    transforms.Resize((28,28)), # Resize to have the same size as MNIST images
                                                                    transforms.ToTensor(),
                                                                    transforms.Normalize((0.1307,), (0.3081,))
                                                                 ])
                                    )
    else:
        raise NotImplementedError("Dataset {} not supported yet".format(dataset))


    # To display an example of image in the dataset

    # sample = 1
    # sample_image, sample_image_label = train_dataset.__getitem__(sample)[0], train_dataset.__getitem__(sample)[1]
    # print("Label first sample: ", sample_image_label)
    # plt.imshow(sample_image.numpy()[0], cmap='gray')
    # plt.show()

    # Creating data loaders

    if (dataset == 'MNIST'):
        train_samples_nb = 1800
        test_samples_nb = 200
    elif (dataset == 'USPS'):
        train_samples_nb = 2000
        test_samples_nb = 500
    else:
        raise NotImplementedError("Dataset {} not supported yet".format(dataset))

    train_indices = random.sample(range(len(train_dataset)), train_samples_nb)
    test_indices = random.sample(range(len(test_dataset)), test_samples_nb)

    # samples_per_classes_train = count_nb_samples_per_class(train_indices, train_dataset)
    # print("Samples per class for training: ", samples_per_classes_train)
    # samples_per_classes_test = count_nb_samples_per_class(test_indices, test_dataset)
    # print("Samples per class for testing: ", samples_per_classes_test)

    train_sampler = SubsetRandomSampler(train_indices)
    test_sampler = SubsetRandomSampler(test_indices)
    train_loader = torch.utils.data.DataLoader(train_dataset,\
                                               batch_size=train_batch_size,\
                                               sampler=train_sampler,\
                                               **kwargs)
    test_loader = torch.utils.data.DataLoader(test_dataset,\
                                              batch_size=test_batch_size,\
                                              sampler=test_sampler,\
                                              **kwargs)
    return train_loader, test_loader


def count_nb_samples_per_class(indices, dataset):
    samples_per_classes = {'0':0, '1':0, '2':0, '3':0, '4':0, '5':0, '6':0, '7':0, '8':0, '9':0}
    for idx in indices:
        _, label = dataset.__getitem__(idx)
        samples_per_classes[str(label)] += 1
    return samples_per_classes

def saveModel(model, nameModelFile="./model_parameters"):
    inc = 0
    while os.path.isdir(nameModelFile + '_' + str(inc) + ".pth" + "/"):
        inc +=1
    nameModelFile = nameModelFile + '_' + str(inc) + ".pth"
    torch.save(model.state_dict(), nameModelFile)
    print("Model saved at: {}".format(nameModelFile))
    return nameModelFile

def loadModel(nameModelFile, device=torch.device("cpu")):
    model = allConv_Net(nb_classes=10, in_channels=1)
    model.load_state_dict(torch.load(nameModelFile))
    model.to(device)
    print("Model loaded from: {}".format(nameModelFile))
    return model

def pretrain(model, nameFileParams="./model_parameters"):
    """
        Pretrain a model using USPS or MNIST
    """
    use_cuda = True
    kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
    if (use_cuda):
        device = torch.device("cuda" if use_cuda else "cpu")
    else:
        device = torch.device("cpu")

    # Data loaders
    dataset = 'MNIST'
    if (dataset == 'MNIST'):
        nb_samples_train = 1800
        nb_samples_test = 200
    elif (dataset == 'USPS'):
        nb_samples_train = 2000
        nb_samples_test = 500
    else:
        raise NotImplementedError("Dataset {} not supported yet".format(dataset))
    train_loader, test_loader = data_load(train_batch_size=50, test_batch_size=50, dataset=dataset, **kwargs)

    # Creating model
    model = model.to(device)
    model.apply(weights_init)

    # Training
    lr = 0.01
    weight_decay = 0.01
    # optimizer = optim.Adadelta(model.parameters(), lr=lr)
    # optimizer = optim.SGD(model.parameters(), lr=lr, momentum=0.9, weight_decay=weight_decay) # weight_decay is for L2 regularization
    optimizer = optim.Adam(model.parameters(), lr=lr, weight_decay=weight_decay)

    # criterion = torch.nn.functional.nll_loss
    criterion = torch.nn.CrossEntropyLoss()

    lr_decay = 1
    epochs = 30
    loss_vals = train(model, device, train_loader, criterion, optimizer, epochs, lr_decay, nb_samples_train)
    test(model, device, test_loader, nb_samples_test)


    # Saving parameters of the model
    nameModelFile = saveModel(model)

    return nameModelFile

def main():
    use_cuda = True
    kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
    if (use_cuda):
        device = torch.device("cuda" if use_cuda else "cpu")
    else:
        device = torch.device("cpu")

    # Data loaders
    dataset = 'MNIST'
    if (dataset == 'MNIST'):
        nb_samples_train = 1800
        nb_samples_test = 200
    elif (dataset == 'USPS'):
        nb_samples_train = 2000
        nb_samples_test = 500
    else:
        raise NotImplementedError("Dataset {} not supported yet".format(dataset))
    train_loader, test_loader = data_load(train_batch_size=50, test_batch_size=50, dataset=dataset, **kwargs)

    # Creating model
    model = allConv_Net(nb_classes=10, in_channels=1).to(device)
    model.apply(weights_init)

    # Training
    lr = 0.01
    # optimizer = optim.Adadelta(model.parameters(), lr=lr)
    # optimizer = optim.SGD(model.parameters(), lr=lr, momentum=0.9, weight_decay=0.01) # weight_decay is for L2 regularization
    optimizer = optim.Adam(model.parameters(), lr=lr, weight_decay=0.01)

    # criterion = torch.nn.functional.nll_loss
    criterion = torch.nn.CrossEntropyLoss()

    lr_decay = 0.8
    epochs = 30
    loss_vals = train(model, device, train_loader, criterion, optimizer, epochs, lr_decay, nb_samples_train)
    test(model, device, test_loader, nb_samples_test)


    # Saving parameters of the model
    nameModelFile = saveModel(model)

    # Load model
    model_2 = loadModel(nameModelFile, device)

    accuracy, precision, recall, f1_score, error_rate, loss = computeMetricsMulticlass(model_2, test_loader, criterion, device=device)
    print("Testing Accuracy: {}%".format(accuracy*100))
    print("Testing Precision: {}%".format(precision*100))
    print("Testing F1_score: {}%".format(f1_score*100))
    print("Testing Error_rate: {}%".format(error_rate*100))


if __name__=="__main__":
    main()
