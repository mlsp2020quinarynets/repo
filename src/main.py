#!/usr/bin/env python3

#######################################
###
### Run a Simulated Annealing algorithm
###
#######################################
from experiment import *

def main():
    """
    def main(): the main function of this script
    """
    print("Starting the experience!!!")
    my_expe_instance = my_expe_class("SA_pretrain_MNIST_train_USPS",\
                                     n_init_positive_gaps = 100,\
                                     n_iter = 600,\
                                     batch_size=100)
    my_expe_instance.run()
    my_expe_instance.plot_results()
    print("Finishing the experience!!!")

if __name__ == "__main__":
    """To enable both running the script or loading it as a module
    """
    main()
