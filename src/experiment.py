#!/usr/bin/env python3

import os
import time
import math
from random import shuffle
import torch
from torch.utils.data import Dataset, DataLoader
from torch.utils.data.sampler import SubsetRandomSampler
from tools import dichotomie_decresent, get_beta_schedule
from matplotlib import pyplot as plt
from metrics import computeMetricsMulticlass
from mnist_usps_train import pretrain, loadModel, data_load
from model import *

ACTIVATE_PRINT = True
ACTIVATE_LOSS = True
ACTIVATE_TIME = True

def printv(str, activ, rank=0):
    """
        Allows to activate or desactivate easily the different prints by just
        setting the global variables at the beginning of the code to false
    """
    if (activ):
        if (rank == 0):
            print(str)

def plotLoss(loss_vals, test_loss_vals):
    """
        Plot the loss over the epochs

        Parameters
        ----------
        loss_vals: array
            Values of the loss to plot
        test_loss_vals: array
            Values of the test loss to plot
        nameLoss: str
            Path and name to save the figure
    """
    plt.figure()
    plt.plot(list(range(len(loss_vals))), loss_vals, label="train loss")
    plt.plot(list(range(len(test_loss_vals))), test_loss_vals, label="test_loss")
    plt.title("Loss")
    plt.xlabel('iteration')
    plt.ylabel('loss')
    plt.legend()
    inc = 0
    while os.path.isfile("./loss_"+str(inc)+'.png'):
        inc +=1
    plt.savefig("./loss_"+str(inc)+'.png')

class my_expe_class(object):
    """
    class my_expe_class(expe_ID)
    Defines the experiment attribute and functions
    INPUT: the experiment ID (str)
    OUTPUT: an instance of the experiment class
    """
    def __init__(self, expe_ID, n_init_positive_gaps = 27200, n_iter = 15000, batch_size=100):
        """
        def __init__(self, expe_ID, n_init_positive_gaps, n_iter)
        During initialisation the state space is explored (with acceptation rate = 1 for any new state)
        Each times that there is a positive variation of the loss function (or energy function), the energy gap is stored in the record_list_positive_loss_gap
        Once the number of positive energy gaps reaches n_init_positive_gaps, the dichotomie_decresent function computes the Beta_inf and Beta_sup values (associated to the initial and final temperature of the Simulated Annealing algorithm)

        Parameters:
        -----------
        exp_ID: str
            Name of the experiment
        n_init_positive_gaps: int
            Number of uphill moves for initialization (M in the paper)
        n_iter: int
            Number of iterations to do for the training. This is equivalent
            to the number parameters that we are going to try to modify to do
            an update (a parameter can be modified several times).
        batch_size: int
        """
        print("Initializing experiment!!!")

        # experiment ID
        self.ID = expe_ID

        # number of iterations for training
        self.n_iter = n_iter

        # Setting device to do computation (CPU or GPU)
        self.device = torch.device('cpu')
        self.use_GPU = False # WARNING: If set a True it gives an error, this problem
        # should be solved soon
        kwargs = {'num_workers': 1, 'pin_memory': True} if self.use_GPU else {}
        if (self.use_GPU):
            self.device = torch.device('cuda')
        self.batch_size = batch_size

        # Datasets
        # Data loaders
        dataset = 'USPS'
        if (dataset == 'MNIST'):
            nb_samples_train = 1800
            nb_samples_test = 200
        elif (dataset == 'USPS'):
            nb_samples_train = 2000
            nb_samples_test = 500
        else:
            raise NotImplementedError("Dataset {} not supported yet".format(dataset))
        self.train_dataloader, self.eval_dataloader = data_load(train_batch_size=self.batch_size, test_batch_size=self.batch_size, dataset=dataset, **kwargs)

        # Creating the lists that will contain the losses
        self.train_loss_values = []
        self.val_loss_values = []

        # Creating list for the metrics
        self.avg_acc, self.avg_precision, self.avg_recall, self.avg_f1_score, self.error_rate = [], [], [], [], []

        # Creating a model instance
        printv("\n=======> Creating instace of the model <=======", ACTIVATE_PRINT, 0)
        self.model = allConv_Net(nb_classes=10, in_channels=1)
        if (self.use_GPU):
            if torch.cuda.device_count() > 1:
              print("Using ", torch.cuda.device_count(), "GPUs")
              self.model = torch.nn.DataParallel(self.model)
        else:
             self.model = self.model.to(self.device)
        printv("=======> Finished Creating instace of the model <=======\n", ACTIVATE_PRINT, 0)

        # Pretraining the model
        printv("\n=======> Starting pretraining model <=======", ACTIVATE_PRINT, 0)
        file_model_parameters = pretrain(self.model)
        printv("=======> Finished pretraining model <=======\n", ACTIVATE_PRINT, 0)

        # Loading the pretrained weights
        self.model = loadModel(file_model_parameters, self.device)
        self.model.freeze_layers()
        self.model.double()

        # parameters used to define the temperature schedule once the Betas are known
        self.acc_rate_start = 0.6
        self.acc_rate_end = 1e-4

        # Dict of acceptation rates where the key is the iteration number and
        # the value is the acceptation rate at that iteration
        self.acceptation_rates_dict = {}

        # Length of each constant stage (here set equal to the number of parameters of the model so that all the parameters are visited with same acceptation rate before updating the temperature/Betas)
        self.K = self.model.n_weights_conv_classifier + self.model.n_gaussian

        # Number of constant temperature stages (computed so that sigma * K gets as close a possible to the total number of iterations n_iter)
        self.sigma = self.n_iter // self.K

        # parameters followed during training
        self.positive_gap_acceptation_history = [] # to estimate the experimental acceptation rate

        # initial loss value
        self.old_loss = self.get_train_loss_average()

        # number of initial positive energy gaps required fot the Beta_inf and Beta_sup estimation
        self.n_init_positive_gaps = n_init_positive_gaps

        # list to store the positive energy gaps
        self.record_list_positive_loss_gap = []

        # temporary counter for the positive energy gaps
        n_detected_positive_gaps = 0

        while n_detected_positive_gaps < self.n_init_positive_gaps:
            startTime = time.time()
            # getting a shuffled list of indices not to access the model parameters in the same order each time they all have been accessed once
            self.shuffle_list_of_param_indices = [['classifier', x] for x in range(self.model.n_weights_conv_classifier)] + \
                                                 [['gaussianActivation', x] for x in range(self.model.n_gaussian)]
            shuffle(self.shuffle_list_of_param_indices)

            startTimefor = time.time()
            # Choosing a random parameter to modify
            iter_init = random.choice(self.shuffle_list_of_param_indices)

            # try a new state
            self.model.try_modifying_a_weight(iter_init)

            # compute the energy/loss value associated to this new state
            startTime_averageLoss = time.time()
            current_loss = self.get_train_loss_average()
            endTime_averageLoss = time.time()
            printv("Time to compute average loss {} s".format(endTime_averageLoss - startTime_averageLoss), ACTIVATE_TIME, 0)
            # during initialisation any new state is accepted which will not be the case during "training"

            # Test if the new state leads to a positive energy gap compared to previous state
            if current_loss > self.old_loss:
                # if positive loss gap detected
                # the positive loss gap is stored
                self.record_list_positive_loss_gap.append(current_loss - self.old_loss)
                # increment counter
                n_detected_positive_gaps += 1
                if (n_detected_positive_gaps == self.n_init_positive_gaps):
                    break
            printv("n_detected_positive_gaps = {}".format(n_detected_positive_gaps), ACTIVATE_PRINT, 0)
            endTime = time.time()
            printv("Init run experiment {} takes {} s".format(n_detected_positive_gaps, endTime - startTime), ACTIVATE_PRINT, 0)
            n_init_positive_gaps += 1

        # compute the Beta values (initial and final temperature) so that the acceptation rate is close to acc_rate_start and acc_rate_end, respectively
        (self.Beta_inf, self.Beta_sup) = dichotomie_decresent(self.record_list_positive_loss_gap,  self.acc_rate_start, self.acc_rate_end)
        printv((self.Beta_inf, self.Beta_sup), True, 0)
        # Computing the cooling sequence
        self.beta_schedule = get_beta_schedule(self.n_iter, self.Beta_inf, self.Beta_sup, self.sigma, self.K)
        printv(len(self.beta_schedule), True, 0)
        printv("Initialization successful", True, 0)
        self.checkpoint_save(name_checkpoint='initialization')

    def get_loss(self, y_pred, labels):
        """
            MSE loss
        """
        pred_labels = []
        for pred in y_pred:
            values, indices = pred.max(0)
            pred_labels.append(indices)
        nb_samples_batch = len(pred_labels)
        pred_labels = torch.tensor(pred_labels)
        loss = (1/2)*math.sqrt(((pred_labels - labels)**2).sum()/float(nb_samples_batch))
        return loss

    def predict(self, x):
        y_pred = self.model(x)
        pred_labels = []
        for pred in y_pred:
            values, indices = pred.max(0)
            pred_labels.append(indices)
        return torch.tensor(pred_labels)

    def get_train_loss_average(self):
        """
            Get the average train loss on the whole dataset with the current parameters

            Returns
            -------
            average_train_loss: float
                Average train loss on the whole dataset in the current state

        """
        # cross_entropy = torch.nn.CrossEntropyLoss()
        tmp_train_loss = 0
        nb_train_batch = 0

        for data, labels in self.train_dataloader:
            data = data.to(self.device)
            labels = labels.to(self.device)
            # Forward Pass

            y_pred = self.model(data.double())
            # tmp_train_loss = cross_entropy(y_pred, labels)
            tmp_train_loss += self.get_loss(y_pred, labels)
            nb_train_batch += 1

        average_train_loss = tmp_train_loss/float(nb_train_batch)
        return average_train_loss

    def get_val_loss_average(self):
        """
            Get the average val loss on the whole dataset with the current parameters

            Returns
            -------
            average_val_loss: float
                Average val loss on the whole dataset in the current state

        """
        # cross_entropy = torch.nn.CrossEntropyLoss()
        tmp_val_loss = 0
        nb_val_batch = 0

        for data, labels in self.eval_dataloader:
            data = data.to(self.device)
            labels = labels.to(self.device)
            # Forward Pass
            y_pred = self.model(data.double())
            # tmp_val_loss = cross_entropy(y_pred, labels)
            tmp_val_loss += self.get_loss(y_pred, labels)
            nb_val_batch += 1

        average_val_loss = tmp_val_loss/float(nb_val_batch)

        return average_val_loss


    def run(self):
        """
        def run(self)
        to run the optimization experiment
        """
        printv("Running experiment: {}".format(str(self.ID)), True, 0)

        # Losses
        training_loss = []

        iter = 0
        while iter < self.n_iter:
            printv("iterarion #{}/{}".format(iter, self.n_iter), True, 0)
            self.nb_weights = self.model.n_weights_conv_classifier + self.model.n_gaussian
            if iter % self.nb_weights == 0:
                # getting a shuffled list of indices not to access the model parameters in the same order each time they all have been accessed once
                self.shuffle_list_of_param_indices = [["classifier", x] for x in range(self.model.n_weights_conv_classifier)] + \
                                                     [["gaussianActivation", x] for x in range(self.model.n_gaussian)]
                shuffle(self.shuffle_list_of_param_indices)

            startTime = time.time()

            # Choosing a random parameter to modify
            idx_weight_modify = random.choice(self.shuffle_list_of_param_indices)

            # try a new state
            self.model.try_modifying_a_weight(idx_weight_modify)

            # compute the energy/loss value associated to this new state
            current_loss = self.get_train_loss_average()

            # Test if the new state leads to a negative energy gap compared to previous state
            if current_loss < self.old_loss:
                # if negative loss gap detected
                # the new state is accepted unconditionally
                self.train_loss_values.append(current_loss)
                self.old_loss = current_loss

            elif torch.rand(1) < torch.exp( - self.beta_schedule[iter] * (current_loss - self.old_loss)):
            # testing if accepting a positive gap (accpetation probability decreases along the Betas/temperature schedule)
                self.train_loss_values.append(current_loss)
                self.old_loss = current_loss
                self.positive_gap_acceptation_history.append(1) # append a one if the new state is accepted

            else:
                # rejecting the modification
                self.model.reject_last_modification()
                self.positive_gap_acceptation_history.append(0) # append a zero if the new state is rejected
                # so the loss value stays the same (state is not modified at this iteration)
                self.train_loss_values.append(self.old_loss)
            endTime = time.time()
            printv("Time for loop training iter {} is {} s".format(iter, endTime - startTime), ACTIVATE_TIME, 0)

            # Training loss information
            printv("training step #{} : training loss = {}".format(iter, current_loss), ACTIVATE_LOSS, 0)
            training_loss.append(current_loss)
            printv("Beta at iter {} is {}".format(iter, self.beta_schedule[iter]), ACTIVATE_PRINT, 0)

            # Evaluation loss and metrics information
            eval_loss = self.get_val_loss_average()
            self.val_loss_values.append(eval_loss)
            printv("evalution at step #{} : average evaluation loss = {}".format(iter,eval_loss), ACTIVATE_LOSS, 0)
            avg_acc, avg_precision, avg_recall, avg_f1_score, error_rate, _= computeMetricsMulticlass(self.model, \
                                                                                           self.eval_dataloader, \
                                                                                           None, \
                                                                                           device=self.device)
            print("Evaluation accuracy: {}\nEvaluation precision: {}".format(avg_acc, avg_precision))
            print("Evaluation recall: {}\nEvaluation f1 score: {}".format(avg_recall, avg_f1_score))
            print("Evaluation error rate: {}\n".format(error_rate))
            self.avg_acc.append(avg_acc)
            self.avg_precision.append(avg_precision)
            self.avg_recall.append(avg_recall)
            self.avg_f1_score.append(avg_f1_score)
            self.error_rate.append(error_rate)


            # Testing if we have good acceptation rates according to self.acc_rate_start
            # and self.acc_rate_end
            accepted_gaps = sum(self.positive_gap_acceptation_history)
            if (iter % 100 == 0 and iter != 0):
                # Doing checkpoint
                self.checkpoint_save(name_checkpoint='training')

                # Acceptation rate
                self.acceptation_rate = float(accepted_gaps)/len(self.positive_gap_acceptation_history)
                self.acceptation_rates_dict[iter] = self.acceptation_rate
                printv("=======> Acceptation rate at iteration {} is {} <=======".format(iter, self.acceptation_rate), True, 0)
                self.positive_gap_acceptation_history = [] # We reset to compute the new acceptation rate later

            # end of iteration
            iter += 1

        # end of training
        self.last_state = self.model

        # Checkpoint final
        self.checkpoint_save(name_checkpoint='final')

        plotLoss(training_loss, self.val_loss_values)

        # Testing on the test data
        # accuracy, precision, recall, f1_score, test_error = computeMetricsMulticlass(self.model, self.test_dataloader)
        # print("\n \nMetrics on the test set: \n accuracy = {} \n precision = {} \n recall = {} \n f1_score = {} \n test_error = {} \n \n".format(accuracy, precision, recall, f1_score, test_error))

    def checkpoint_save(self, name_checkpoint=''):
        checkpointName = "./checkpoint_" + name_checkpoint + '_' + self.ID + ".pth"
        torch.save({
            'batch_size': self.batch_size,
            'train_loss_values': self.train_loss_values,
            'val_loss_values': self.val_loss_values,
            'model_state_dict': self.model.state_dict(),
            'acc_rate_start': self.acc_rate_start,\
            'acc_rate_end': self.acc_rate_end,\
            'acceptation_rates_dict': self.acceptation_rates_dict,\
            'K': self.K,\
            'sigma': self.sigma,\
            'positive_gap_acceptation_history': self.positive_gap_acceptation_history,\
            'old_loss': self.old_loss,\
            'n_init_positive_gaps': self.n_init_positive_gaps,\
            'record_list_positive_loss_gap': self.record_list_positive_loss_gap,\
            'shuffle_list_of_param_indices': self.shuffle_list_of_param_indices,\
            'Beta_inf': self.Beta_inf,\
            'Beta_sup': self.Beta_sup,\
            'beta_schedule': self.beta_schedule,\
            'avg_acc': self.avg_acc,
            'avg_precision': self.avg_precision,
            'avg_recall': self.avg_recall,
            'avg_f1_score': self.avg_f1_score,
            'error_rate': self.error_rate
            }, checkpointName)

    def plot_results(self):
        """
        def plot_results(self)
        to plot the experiment indicators
        """
        plt.ioff()
        plt.figure()
        plt.plot(list(range(len(self.train_loss_values))), self.train_loss_values, label = "train loss")
        plt.ylabel("loss value")
        plt.xlabel("training steps")
        plt.legend()
        inc = 0
        while os.path.isfile("./loss_train_"+str(inc)+'.png'):
            inc +=1
        plt.savefig("./loss_train_"+str(inc)+'.png')
        # plt.show()
