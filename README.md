A figure describing the architecture that we used
can be found on the directory Img

To run the experiment follow the following steps:
- Make sure that you have all the needed libraries.
- If not use "pip install -r requirements.txt" or
  "pip3 install -r requirements.txt"
- Go to the src directory and execute main.py
- You can change three parameters in the main.py
  script: the number of uphill moves, the number 
  of iterations for training and the batch size.
- The script will generate several files:
	- Two .png files, one containing the training 
	  and evaluation losses and another one with
	  just the training loss.
	- Three .pth files, one for the initialized model,
	  one for an intermediate model and another one 
	  with the final model. 
